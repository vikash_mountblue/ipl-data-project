const express = require("express");
const app = express();
const path = require("path");
const dataBase = require("./config");
const csv = require("../../csvtojson");
let matchData = csv.convertmyfile(__dirname + "/../data/matches.csv");
let deliveryData = csv.convertmyfile(__dirname + "/../data/deliveries.csv");
const fs = require("fs");
const {
  matchesPerYear,
  matchesWonPerTeam,
  extraRun2016,
  economicalBowler2015,
} = require("./iplSql");

// for insert data into Database convert into files convert into Array
function convertIntoArray(data) {
  let ArrayData = data.reduce((accumulator, element) => {
    accumulator.push(Object.values(element));
    return accumulator;
  }, []);
  return ArrayData;
}
let matchArrayData = convertIntoArray(matchData);
let deliveryArrayData = convertIntoArray(deliveryData);

// Load data into database
function loadDataIntoDatabase(drop, create, query, values) {
  return new Promise((resolve, reject) => {
    dataBase.connect((error) => {
      if (error) {
        reject("Database connection fail", error);
      }
      dataBase.query(drop, (error) => {
        if (error) {
          reject(`Drop ${drop} table Failed`, error);
        }
      });
      dataBase.query(create, (error) => {
        if (error) {
          reject(`Create ${drop} table Failed`, error);
        }
      });
      dataBase.query(query, values, (error, results, fields) => {
        if (error) {
          reject(error);
        }
        resolve(results);
      });
      dataBase.end();
    });
  });
}

// query for create table or delete if table exist
async function loadMatchesData() {
  const drop = `DROP TABLE IF EXISTS matches`;
  const createTable =
    "CREATE TABLE matches(id int,season int,city varchar(30),date date,team1 varchar(50),team2 varchar(50),toss_winner varchar(50),toss_decision varchar(20),result varchar(20),dl_applied int,winner varchar(50),win_by_runs int,win_by_wickets int,player_of_match varchar(50),venue varchar(100),umpire1 varchar(40),umpire2 varchar(40),umpire3 varchar(40))";
  const matchesDataInsertQuery = `insert into matches values ?`;
  const result = await loadDataIntoDatabase(
    drop,
    createTable,
    matchesDataInsertQuery,
    [matchArrayData]
  );
  console.log("matches database", result);
}
loadMatchesData();

async function loadDeliveryData() {
  const drop = `DROP TABLE IF EXISTS deliveries`;
  const createTable =
    "CREATE TABLE deliveries(match_id int,inning int,batting_team varchar(50),bowling_team varchar(50),overs int,ball int,batsman varchar(50),non_striker varchar(50),bowler varchar(50),is_super_over int,wide_runs int,bye_runs int,legbye_runs int,noball_runs int,penalty_runs int,batsman_runs int,extra_runs int,total_runs int,player_dismissed varchar(50),dismissal_kind varchar(50),fielder varchar(50))";
  const deliveyDataInsertQuery = `insert into deliveries values ?`;
  const result = await loadDataIntoDatabase(
    drop,
    createTable,
    deliveyDataInsertQuery,
    [deliveryArrayData]
  );
  console.log("deliveries database", result);
}
loadDeliveryData();

// Data write into json file
function convertToJson(data, filePath) {
  fs.writeFileSync(filePath, JSON.stringify(data, null, 2));
}

// sql query for matches per year
function matchesPerYearDatas() {
  const queryForMatchesPerYear = `select season, count(id) matches from matches group by season`;
  let matchesPerYearData = matchesPerYear(dataBase, queryForMatchesPerYear);
  matchesPerYearData
    .then((element) => {
      let matchTotal = {};
      matchTotal["matchTotal"] = element;
      convertToJson(matchTotal, __dirname + "/../output/matchesPerYear.json");
    })
    .catch((error) => {
      console.log(error);
    });
}
matchesPerYearDatas();

// sql query for matches won per year per team
function matchesWonPerTeams() {
  const queryForMatchesWonPerTeam = `select season,winner, count(id) wins from matches group by season, winner`;
  let matchesWonPerTeamData = matchesWonPerTeam(
    dataBase,
    queryForMatchesWonPerTeam
  );
  matchesWonPerTeamData
    .then((element) => {
      let winningCountMatches = {};
      winningCountMatches["winCount"] = element;
      convertToJson(
        winningCountMatches,
        __dirname + "/../output/matchesWonPerTeam.json"
      );
    })
    .catch((error) => {
      console.log(error);
    });
}
matchesWonPerTeams();

// sql query for Extra run in 2016
function extraRuns() {
  const queryForExtraRun = `select bowling_team, sum(extra_runs) as extraRun from deliveries where exists (select id from matches where season=2016 and id=match_id) group by bowling_team`;
  let extraRun = extraRun2016(dataBase, queryForExtraRun);
  extraRun
    .then((element) => {
      let extra_run = {};
      extra_run["extrasCount"] = element;
      convertToJson(extra_run, __dirname + "/../output/extraRun.json");
    })
    .catch((error) => {
      console.log(error);
    });
}
extraRuns();

// sql query for Economy Rate in 2015
function economyRates() {
  const queryForEconomyRate = `select bowler, sum(total_runs)/(count(ball)/6) as economyRate from deliveries where exists (select id from matches where season=2015 and id=match_id) group by bowler order by economyRate limit 10`;
  let economyRate = economicalBowler2015(dataBase, queryForEconomyRate);
  economyRate
    .then((element) => {
      let economicBowlersRate = {};
      economicBowlersRate["economicalBowlers"] = element;
      convertToJson(
        economicBowlersRate,
        __dirname + "/../output/economicBowlers.json"
      );
    })
    .catch((error) => {
      console.log(error);
    });
}
economyRates();
app.use(express.static(path.join(__dirname + "/../client")));
app.use(express.static(path.join(__dirname + "/../output")));
app.listen(3000, function () {
  console.log("Web app listening on port 3000!");
});
