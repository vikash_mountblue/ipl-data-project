function sqlQuestionQuery(dataBase, query) {
  return new Promise((resolve, reject) => {
    dataBase.connect((error) => {
      if (error) {
        reject(error);
      }
      dataBase.query(query, (error, results, fields) => {
        if (error) {
          reject(error);
        }
        // console.log("data2", results);
        resolve(results);
      });
      dataBase.end();
    });
  });
}

// match per year
function matchesPerYear(dataBase, query) {
  return sqlQuestionQuery(dataBase, query).then((resolve) => {
    let value = resolve.reduce((accumulator, element) => {
      accumulator[element.season] = element.matches;
      return accumulator;
    }, {});
    // console.log("matchesPerYear result", value);
    return value;
  });
}

// matches won per team per year
function matchesWonPerTeam(dataBase, query) {
  return sqlQuestionQuery(dataBase, query).then((resolve) => {
    let value = resolve.reduce((accumulator, element) => {
      let allTeam = {};
      if (accumulator[element.winner] === undefined) {
      } else {
        allTeam = accumulator[element.winner];
      }
      allTeam[element.season] = element.wins;
      accumulator[element.winner] = allTeam;
      return accumulator;
    }, {});
    // console.log("matchesWonPerYear result", value);
    return value;
  });
}

// Extra run in 2016
function extraRun2016(dataBase, query) {
  return sqlQuestionQuery(dataBase, query).then((resolve) => {
    let value = resolve.reduce((accumulator, element) => {
      accumulator[element.bowling_team] = element.extraRun;
      return accumulator;
    }, {});
    // console.log("extra run result", value);
    return value;
  });
}

// Economical bowler
function economicalBowler2015(dataBase, query) {
  return sqlQuestionQuery(dataBase, query).then((resolve) => {
    let value = resolve.reduce((accumulator, element) => {
      accumulator[element.bowler] = element.economyRate;
      return accumulator;
    }, {});
    // console.log("economy rate result", value);
    return value;
  });
}

module.exports = {
  matchesPerYear,
  matchesWonPerTeam,
  extraRun2016,
  economicalBowler2015,
};
