const dotenv = require("dotenv");
const mysql = require("mysql2");
dotenv.config();
const connection = mysql.createConnection({
  host: process.env.HOST,
  user: process.env.DB,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
});
module.exports = connection;
