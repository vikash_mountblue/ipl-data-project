const csv = require("../../csvtojson");
let matchData = csv.convertmyfile("../data/matches.csv");
let deliveryData = csv.convertmyfile("../data/deliveries.csv");
const fs = require("fs");
// 1. Number of matches played per year for all the years in IPL.
function matchesTotal() {
  return matchData.reduce((matchesInSeason, match) => {
    if (matchesInSeason.hasOwnProperty(match["season"])) {
      matchesInSeason[match["season"]] += 1;
      return matchesInSeason;
    } else {
      matchesInSeason[match["season"]] = 1;
      return matchesInSeason;
    }
  }, {});
}
// console.log("matches per year", matchesTotal());

// 2. Number of matches won of per team per year in IPL.
function winningCount() {
  return matchData.reduce((winsInSeason, match) => {
    if (winsInSeason.hasOwnProperty(match["winner"])) {
      if (winsInSeason[match["winner"]].hasOwnProperty(match["season"])) {
        winsInSeason[match["winner"]][match["season"]] += 1;
      } else {
        if (match["winner"] !== "") {
          winsInSeason[match["winner"]][match["season"]] = 1;
        }
      }
      return winsInSeason;
    } else {
      if (match["winner"] !== "") {
        winsInSeason[match["winner"]] = {};
        winsInSeason[match["winner"]][match["season"]] = 1;
      }
      return winsInSeason;
    }
  }, {});
}

// console.log("number of matches won per team/year", winningCount());

// function to get deliveries file data and matches file and filter data for require matchid
function getDeliveries(season) {
  let matchIds = matchData.reduce((matchId, match) => {
    if (match["season"] === season) {
      matchId.push(match["id"]);
    }
    return matchId;
  }, []);
  // console.log('match id ', matchIds)
  return deliveryData.filter((deliveries) => {
    if (matchIds.includes(deliveries["match_id"])) {
      return deliveries;
    }
  });
}

// 3. Extra runs conceded per team in 2016
function extras() {
  return getDeliveries(2016).reduce((extrasPerTeam, delivary) => {
    if (extrasPerTeam.hasOwnProperty(delivary["bowling_team"])) {
      extrasPerTeam[delivary["bowling_team"]] += delivary["extra_runs"];
    } else {
      extrasPerTeam[delivary["bowling_team"]] = delivary["extra_runs"];
    }
    return extrasPerTeam;
  }, {});
}

// console.log("extra run per team in 2016", extras());

// 4. Top 10 economical bowlers in 2015
function economicBowlers() {
  let bowlerObj = getDeliveries(2015).reduce((bowlerStats, deliveries) => {
    if (bowlerStats.hasOwnProperty(deliveries["bowler"])) {
      bowlerStats[deliveries["bowler"]]["runs"] += deliveries["total_runs"];
      if (deliveries["ball"] === 1) {
        bowlerStats[deliveries["bowler"]]["overs"] += 1;
      }
      return bowlerStats;
    } else {
      bowlerStats[deliveries["bowler"]] = {};
      bowlerStats[deliveries["bowler"]]["runs"] = deliveries["total_runs"];
      bowlerStats[deliveries["bowler"]]["overs"] = 1;
      return bowlerStats;
    }
  }, {});
  let bowlerArr = Object.keys(bowlerObj);
  let ecoBowlersObj = bowlerArr.reduce((bowlerName, bowlerEconomy) => {
    bowlerName[
      Math.round(
        (bowlerObj[bowlerEconomy]["runs"] / bowlerObj[bowlerEconomy]["overs"]) *
          100
      ) / 100
    ] = bowlerEconomy;
    return bowlerName;
  }, {});
  let ecoBowlersArr = Object.keys(ecoBowlersObj);
  return ecoBowlersArr
    .map((economy) => parseFloat(economy))
    .sort((a, b) => a - b)
    .slice(0, 10)
    .reduce((bowlerEconomy, economy) => {
      bowlerEconomy[ecoBowlersObj[economy]] = economy;
      return bowlerEconomy;
    }, {});
}
// console.log("ecomical bowlwe in 2015", economicBowlers());

// Write data into JSON file
function convertToJson() {
  let matchTotal = {};
  let winningCountMatches = {};
  let extra_runs = {};
  let economicBowlersRate = {};

  matchTotal["matchTotal"] = matchesTotal();
  winningCountMatches["winCount"] = winningCount();
  extra_runs["extrasCount"] = extras();
  economicBowlersRate["economicalBowlers"] = economicBowlers();

  fs.writeFileSync(
    "../output/matchesPerYear.json",
    JSON.stringify(matchTotal, null, 2)
  );
  fs.writeFileSync(
    "../output/matchesWonPerTeam.json",
    JSON.stringify(winningCountMatches, null, 2)
  );
  fs.writeFileSync(
    "../output/extraRun.json",
    JSON.stringify(extra_runs, null, 2)
  );
  fs.writeFileSync(
    "../output/economicBowlers.json",
    JSON.stringify(economicBowlersRate, null, 2)
  );
}

convertToJson();
